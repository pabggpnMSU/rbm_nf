#!/usr/bin/env python

# Libraries
from functools import partial
import pickle
import sys
from typing import Callable

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp
import numpy as np
import optax

from models import PG2d

# Specify to use CPU, not GPU
jax.config.update('jax_platform_name', 'cpu')

def Identity(x):
    return x

# Class linear transformation with arbitrary initialization of weight and bias
class Linear(eqx.Module):
    weight: jnp.array
    bias: jnp.array

    def __init__(self, in_size, out_size, initweight, initbias):
        shape_w = np.shape(initweight)
        shape_b = np.shape(initbias)
        if shape_w[0] != out_size or shape_w[1] != in_size or shape_b[0] != out_size:
            print('Input weight of bias does not match the dimention of the model')
            #TODO I want the object to be NOT created.
            exit()
        self.weight = initweight
        self.bias = initbias

    def __call__(self, x):
        return self.weight @ x + self.bias

class MLP(eqx.Module):
    in_size: int
    out_size: int
    depth: int
    width: int
    activation: Callable 
    final_activation: Callable
    layers: list

    def __init__(self, ind, outd, depth, width, actf, final_actf, key):
        self.in_size = ind
        self.out_size = outd
        self.depth = depth
        self.width = width
        self.activation = actf
        self.final_activation = final_actf
        W = ind * width
        keys = jax.random.split(key, depth)
        self.layers = []
        if depth == 0:
            self.layers.append(Linear(ind, outd, jnp.zeros((outd, ind)), jnp.zeros(outd)))
        else:
            self.layers.append(Linear(ind, W, jax.random.normal(keys[0],(W,ind))/np.sqrt(ind), jnp.zeros(W)))
            for i in range(1,depth):
                self.layers.append(Linear(W, W, jax.random.normal(keys[i],(W,W))/np.sqrt(ind), jnp.zeros(W)))
            self.layers.append(Linear(W, outd, jnp.zeros((outd,W)), jnp.zeros(outd)))

    def __call__(self, x):
        for layer in self.layers[:-1]:
            x = layer(x)
            x = self.activation(x)
        x = self.final_activation(x)
        x = self.layers[-1](x)
        return x

# Flow with MLP, no guarantee for positive Jacobian
class NaiveFlow(eqx.Module):
    norm: np.float32
    depth: int
    width: int
    asymptotic: Linear
    mlp: MLP

    def __init__(self, key, depth, width, D):
        self.norm = jnp.array(0.)
        self.depth = depth
        self.width = width
        W = D * width
        self.asymptotic = Linear(D, D, jnp.eye(D), jnp.zeros(D))
        self.mlp = MLP(D, D, depth, width, jax.nn.selu, jax.nn.sigmoid, key)

    def __call__(self, x):
        xasmp = self.asymptotic(x)
        xmlp = self.mlp(x)
        return xasmp + xmlp

# Affine coupling layer
class AffineCoupling(eqx.Module):
    mask: jnp.array
    scale: MLP
    trans: Linear

    def __init__(self, key, D, mask=None):
        maskKey, scaleKey = jax.random.split(key)
        if mask is None:
            self.mask = jax.random.randint(maskKey, (D,), 0, 2)
        else:
            self.mask = mask
        self.scale = MLP(D, D, 1, 1, jnp.tanh, Identity, scaleKey)
        self.trans = Linear(D, D, jnp.zeros((D,D)), jnp.zeros(D))

    def map(self, x):
        return self(x)[0]

    # Returns the output and log of Jacobian
    def __call__(self, x):
        y = self.mask * x
        s = self.scale(y)
        t = self.trans(y)
        z = y + (1-self.mask) * (jnp.exp(s)*x + t)
        return z, jnp.sum((1-self.mask) * s)

class CheckeredAffines(eqx.Module):
    even: AffineCoupling
    odd: AffineCoupling

    def __init__(self, key, D):
        key_even, key_odd = jax.random.split(key)
        mask_even = jnp.arange(D)%2
        mask_odd = 1 - mask_even
        self.even = AffineCoupling(key_even, D, mask=mask_even)
        self.odd = AffineCoupling(key_odd, D, mask=mask_odd)

    def map(self, x):
        return self(x)[0]

    def __call__(self, x):
        x, ld_e = self.even(x)
        x, ld_o = self.odd(x)
        return x, ld_e + ld_o

class RealNVP(eqx.Module):
    layers: list

    def __init__(self, key, D, depth):
        keys = jax.random.split(key, depth)
        self.layers = [CheckeredAffines(k, D) for k in keys]

    def map(self, x):
        return self(x)[0]


    def __call__(self, x):
        ld = 0
        for l in self.layers:
            x, lj = l(x)
            ld += lj
        return x, ld

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
            description="Training normalizing flow",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@'
            )
    parser.add_argument('model', type=str, help="model filename")
    parser.add_argument('nf', type=str, help="normalizing flow filename")
    parser.add_argument('-n', '--nsamples', type=int, default=100, help="number of samples taken per training")
    parser.add_argument('-l', '--layers', type=int, default=0, help="number of (hidden) layers")
    parser.add_argument('-w', '--width', type=int, default=1, help="width of the internal layers")
    parser.add_argument('-t', '--threshold', type=float, default=-10, help="loss value to achieve before the termination")
    parser.add_argument('-s', '--trainsteps', type=int, default=1e6, help="number of tarin steps taken before the termination")
    parser.add_argument('-r', '--rate', type=float, default=1e-3, help="learning rate")

    args = parser.parse_args()
    jax.config.update("jax_debug_nans", True)

    with open(args.model, 'rb') as f:
        model = pickle.load(f)
    D = model.dim

    sampleKey, initKey = jax.random.split(jax.random.PRNGKey(2))
    ns = args.nsamples

    flow = RealNVP(initKey, D, args.layers)

    # Returns the effective density function after the flow is applied
    @partial(jnp.vectorize, signature='(i)->(),()', excluded={1})
    def gauss_to_dist(x, flow):
        y, logdet = flow(x)
        dist = model.dist(y)
        return dist + logdet, -jnp.sum(x**2)/2

    # Loss function, computed with the distribution (instead of Gaussian)
    def loss(flow, x):
        dist, gauss = gauss_to_dist(x, flow)
        loss = dist - gauss # NF for normalized distribution via KL divergence
        reweight = jnp.exp(dist - gauss)
        return jnp.sum(loss*reweight)/jnp.sum(reweight)

    # Compute the loss function and its derivative wrt NN parameteres
    loss_grad = eqx.filter_jit(eqx.filter_value_and_grad(loss))

    # Initialize the optimizer
    opt = optax.adam(args.rate)
    opt_state = opt.init(eqx.filter(flow, eqx.is_array))

    # Train Normalizing flow for the input model
    train_step = 0
    try:
        loss = 1e2
        while loss > args.threshold and train_step < args.trainsteps:
            sampleKey, sKey = jax.random.split(sampleKey)
            x = jax.random.normal(sampleKey, (ns,D))
            loss, grad = loss_grad(flow, x)
            updates, opt_state = eqx.filter_jit(opt.update)(grad, opt_state)
            flow = eqx.filter_jit(eqx.apply_updates)(flow, updates)
            train_step += 1
            print(f'{train_step} {loss:.3f}')
        eqx.tree_serialise_leaves(args.nf, flow)
    except KeyboardInterrupt:
        pass
        eqx.tree_serialise_leaves(args.nf, flow)


