#!/usr/bin/env python

import argparse
import ast
import pickle
import sys

from models import PG2d

parser = argparse.ArgumentParser(
        description="""Define a model""",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        fromfile_prefix_chars='@'
)
parser.add_argument('filename', type=str, help='model filename')
subparsers = parser.add_subparsers(
        title="Models", help='Type of theory to define', required=True)

def pg2d(args):
    model = PG2d.Model(args.c, args.c1, args.c2, args.c3, args.c4, args.c5, args.c6)
    return model

parser_pg2d = subparsers.add_parser('PG2d', help='2D Giuliani distribution')
parser_pg2d.add_argument('c', type=float, help='location of the "center" of distribution')
parser_pg2d.add_argument('c1', type=float, help='coefficient for the term (x0-c)^2')
parser_pg2d.add_argument('c2', type=float, help='coefficient for the term (x0-c)(x1-c)')
parser_pg2d.add_argument('c3', type=float, help='coefficient for the term (x1-c)^2')
parser_pg2d.add_argument('c4', type=float, help='coefficient for the term (x0-c)^4')
parser_pg2d.add_argument('c5', type=float, help='coefficient for the term (x1-c)^4')
parser_pg2d.add_argument('c6', type=float, help='coefficient for the term (x0-c)(x1-c)^3')
parser_pg2d.set_defaults(func=pg2d)


args  = parser.parse_args()
model = args.func(args)


with open(args.filename, 'wb') as f:
    pickle.dump(model, f)

