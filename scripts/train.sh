#!/bin/sh

mkdir -p data/4_27_2023

model='data/model_1_2_3_4p5_3_3_5.pickle'
dir='data/4_27_2023/'

for l in 2 4 6 8; do
	for s in 500 1000 1500 2000 2500 3000 3500 4000 4500 5000; do
		./nf.py $model ${dir}/nf_l${l}_s${s}.pickle -l $l -t -10 -s $s
	done
done
