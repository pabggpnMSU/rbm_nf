#!/bin/sh

mkdir -p data/4_20_2023

model='data/model_1_2_3_4p5_3_3_5.pickle'
dir='data/4_27_2023/'
B=20
S=100000
l=6
s=5000

for S in 1000 3000 10000 30000 100000; do
	./sample.py $model ${dir}/nf_l${l}_s${s}.pickle $l -B $B -S $S --seed-time
done
