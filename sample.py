#!/usr/bin/env python

# Libraries
import argparse
from functools import partial
import pickle
import sys
import time

import equinox as eqx
import jax
import jax.numpy as jnp
import numpy as np

from models import PG2d
import nf
import nf_inv

parser = argparse.ArgumentParser(
        description="Training normalizing flow",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        fromfile_prefix_chars='@'
        )
parser.add_argument('model', type=str, help="model filename")
parser.add_argument('nf', type=str, help="normalizing flow filename")
parser.add_argument('depth', type=int, help="number of CheckeredAffines in normalizing flow")
parser.add_argument('-B', '--batches', default=-1, type=int, help="number of batches before termination")
parser.add_argument('-S', '--samples', default=1<<12, type=int, help="number of samples per batch")
parser.add_argument('-R', '--reweight', action='store_true', help="perform reweighting" )
parser.add_argument('--seed', type=int, default=0, help="random seed for sampling")
parser.add_argument('--seed-time', action='store_true', help="seed PRNG with current time")
parser.add_argument('--inv', action='store_true', help="samples using NF from nf-inv.py")
parser.add_argument('--label', type=int, help='label')

args = parser.parse_args()

seed = args.seed
if args.seed_time:
    seed = time.time_ns()
sampleKey = jax.random.PRNGKey(seed)

with open(args.model, 'rb') as f:
    model = pickle.load(f)

# TODO Add warning when the size of the model does not match layer and width
if args.inv:
    flow = nf_inv.RealNVP(sampleKey, model.dim, args.depth)
else:
    flow = nf.RealNVP(sampleKey, model.dim, args.depth)
flow = eqx.tree_deserialise_leaves(args.nf, flow)


@partial(jnp.vectorize, signature='(i)->(i),()', excluded={1})
def dist_effective(x, flow):
    y, logdet = flow(x)
    dist = model.dist(y)
    return y, dist + logdet

@jax.jit
def sample(key):
    skey, key = jax.random.split(key)
    x = jax.random.normal(key, (args.samples,model.dim))
    y, dist = dist_effective(x, flow)
    obs = jax.vmap(model.observables)(y)
    rw = jnp.exp(jnp.sum(x**2/2, axis=-1) + dist) * np.power(2*np.pi, model.dim/2)
    norm = np.mean(rw)
    obss = jax.vmap(model.observables)(y)
    if not args.reweight:
        rw = jnp.ones(args.samples)
    obs_rw = jnp.einsum('ij,i->j', obss, rw)
    obs = obs_rw / jnp.sum(rw)
    return norm, *obs

rws = []
obss = []
for _ in range(args.batches):
    sampleKey, pkey = jax.random.split(sampleKey)
    rw, obs1, obs2, obs3 = sample(pkey)
    rws.append(rw)
    obss.append(obs3)
    #print(rw, *obs)
rws = np.array(rws)
obss = np.array(obss)
print(f'{args.label} {np.mean(obss):.3f} {np.var(obss)}')

    









