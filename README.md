# Accelerating sampling on posterior distributions via normalizing flows

## Papers
If using code, or code derived from it, it may be appropriate to cite one or more of the following papers.


## Requirements
The code in this repository requires a resonably up-to-date python3. The main packages used are `jax`, `equinox`, `optax`. The required packages are listed in detail in requirements.txt. To install all libraries using `venv`, do:
```
python -m venv env
. env/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

## Workflow
To start, generate a distribution model with `mkmodel.py`. Then, generate and train a normalizing flow for the model with `nf.py`. Finally, collect samples from the model distribution by `sample.py`.

Here is an example:
```
mkdir -p data/
./mkmodel.py data/model.pickle PG2d 1. 2. 3. 4.5 3. 3. 5.
./nf.py data/model.pickle data/nf.pickle -l 1 -w 1 -t -10
./sample.py data/model.pickle data/nf.pickle 1 -B 10 -S 1000
```
In this example, the PG2d distribution is created first. A normalizing flow is trained with one layer of Real NVP neural network such that the loss function gets down until -10. Since the KL divergence is used as the loss function, the loss can be negative depending on the normalization of the distribution. Then sampling is performed 10 times, each with 1000 samples. To see more options, pass in `-h` for each script.

## Organization

The main scripts are:

- **`mkmodel.py`**: Defines a model
- **`nf.py`**: Trains a normalizing flow
- **`sample.py`**: Samples from a normalizing flow

The directory structure is:

- **`models/`**:Model distributions
