from dataclasses import dataclass
from typing import Tuple
import numpy as np
import jax.numpy as jnp

@dataclass
class Model:
    c: float
    c1: float
    c2: float
    c3: float
    c4: float
    c5: float
    c6: float
    dim = 2

    # returns the log of the distribution
    def dist(self, x):
        y0 = x[0] - self.c
        y1 = x[1] - self.c
        log = -self.c1*y0**2 - self.c2*y0*y1 - self.c3*y1**2 - self.c4*y0**4 - self.c5*y1**4 - self.c6*y0*y1**3
        return log

    def observables(self, x):
        return jnp.array([x[0], x[1], x[0]**2 + x[1]**2])

